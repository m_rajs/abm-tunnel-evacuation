﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace abm_tunnel_evacuation
{
    public enum ENeighbour { N = 0, NE, E, SE, S, SW, W, NW }
    public enum ECellType { clear, obstacle, smokeSource, exit, pedestrian}

    public class TunnelCell
    {
        public List<TunnelCell> Neighbours;
        public double SmokeConcentration;
        public double DistanceToExit;
        public double DistanceToObstacle;
        public bool PedestrianPresent;
        public int X_pos, Y_pos;

        private ECellType cellType;
        private double nextSmokeConcentration;

        public TunnelCell()
        {
            CellType = ECellType.clear;
            Neighbours = new List<TunnelCell>();
            DistanceToExit = Constants.MaxDistanceToExit;
            DistanceToObstacle = Constants.MaxDistanceToObstacle;
            PedestrianPresent = false;
        }

        public ECellType CellType
        {
            get { return cellType; }
            set
            {
                cellType = value;
                if (value == ECellType.smokeSource)
                {
                    SmokeConcentration = 1.0;
                }
                else
                {
                    SmokeConcentration = 0.0;
                }
            }
        }

        public List<TunnelCell> DiagonalNeighbours
        {
            get { return Neighbours.Where(x => x.X_pos != X_pos && x.Y_pos != Y_pos).ToList(); }
        }

        public List<TunnelCell> PerpendicularNeighbours
        {
            get { return Neighbours.Where(x => x.X_pos == X_pos || x.Y_pos == Y_pos).ToList(); }
        }

        public bool CalculateDistanceToExit()
        {
            if (CellType == ECellType.obstacle || CellType == ECellType.exit)
            {
                return false;
            }
            TunnelCell bestDiagonal = DiagonalNeighbours
                ?.OrderBy(x => x.DistanceToExit)
                ?.First();
            TunnelCell bestPerpendicular = PerpendicularNeighbours
                ?.OrderBy(x => x.DistanceToExit)
                ?.First();
            bool distanceChanged = false;
            if (DistanceToExit > bestDiagonal.DistanceToExit + Math.Sqrt(2))
            {
                DistanceToExit = bestDiagonal.DistanceToExit + Math.Sqrt(2);
                distanceChanged = true;
            }
            if (DistanceToExit > bestPerpendicular.DistanceToExit + 1)
            {
                DistanceToExit = bestPerpendicular.DistanceToExit + 1;
                distanceChanged = true;
            }
            return distanceChanged;
        }

        public bool CalculateDistanceToObstacle()
        {
            if (CellType == ECellType.obstacle)
            {
                return false;
            }
            TunnelCell bestDiagonal = DiagonalNeighbours
                ?.OrderBy(x => x.DistanceToObstacle)
                ?.First();
            TunnelCell bestPerpendicular = PerpendicularNeighbours
                ?.OrderBy(x => x.DistanceToObstacle)
                ?.First();
            bool distanceChanged = false;
            if (DistanceToObstacle > bestDiagonal.DistanceToObstacle + Math.Sqrt(2))
            {
                DistanceToObstacle = bestDiagonal.DistanceToObstacle + Math.Sqrt(2);
                distanceChanged = true;
            }
            if (DistanceToObstacle > bestPerpendicular.DistanceToObstacle + 1)
            {
                DistanceToObstacle = bestPerpendicular.DistanceToObstacle + 1;
                distanceChanged = true;
            }
            return distanceChanged;
        }

        public void CalculateNextSmokeConcentration()
        {
            nextSmokeConcentration = SmokeConcentration;
            if (CellType == ECellType.clear)
            {
                List<TunnelCell> higherConcentrations = Neighbours.Where(neighbour => neighbour.SmokeConcentration > SmokeConcentration).ToList();
                if (higherConcentrations.Count() > 0)
                {
                    nextSmokeConcentration = higherConcentrations.Sum(neighbour => neighbour.SmokeConcentration);
                    nextSmokeConcentration += SmokeConcentration / Constants.SmokeVelocity;
                    nextSmokeConcentration /= (higherConcentrations.Count() + (1 / Constants.SmokeVelocity));
                }
            }
        }

        public void UpdateSmokeConcentration()
        {
            SmokeConcentration = nextSmokeConcentration;
        }

        public double IsPedestrianNearby()
        {
            if (Neighbours.Where(x => x.PedestrianPresent).Any())
            {
                return 1.0;
            } else
            {
                return 0.0;
            }
        }
    }
}