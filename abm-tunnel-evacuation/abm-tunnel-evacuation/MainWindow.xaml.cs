﻿using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace abm_tunnel_evacuation
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal Tunnel Tunnel { get; }
        public Border[,] CellsButtons { get; private set; }
        private bool paused;

        public MainWindow()
        {
            paused = false;
            InitializeComponent();     
            InitializeUI();
            Tunnel = Tunnel.GetTunnel();
            InitializeBoard();
            InitializeLog();
        }

        public void LogPedestrian(Pedestrian pedestrian)
        {
            File.AppendAllText(Constants.LogPath, String.Format("{0:0.00}\n", pedestrian.TimeInSimulation));
        }

        private void InitializeUI()
        {
            CellTypeChoice.ItemsSource = Enum.GetValues(typeof(ECellType)).Cast<ECellType>();
            CellTypeChoice.SelectedItem = ECellType.clear;
        }

        private void InitializeBoard()
        {
            Board.Height = Constants.BoardHeight;
            Board.Width = Constants.BoardWidth;

            Board.Rows = Constants.Rows;
            Board.Columns = Constants.Columns;

            CellsButtons = new Border[Constants.Rows, Constants.Columns];

            for (int row = 0; row < Constants.Rows; row++)
            {
                for (int column = 0; column < Constants.Columns; column++)
                {
                    Border cellButton = new Border
                    {
                        BorderBrush = Brushes.Black,
                        BorderThickness = new Thickness(0.25),
                        Background = Brushes.White
                    };

                    cellButton.SetValue(Grid.RowProperty, row);
                    cellButton.SetValue(Grid.ColumnProperty, column);

                    cellButton.MouseLeftButtonDown += new MouseButtonEventHandler(CellButtonLeftMouseDown);
                    cellButton.MouseEnter += new MouseEventHandler(CellButtonMouseEnter);

                    Board.Children.Add(cellButton);

                    CellsButtons[row, column] = cellButton;
                }
            }
        }

        private void InitializeLog()
        {
            if (File.Exists(Constants.LogPath))
            {
                File.Delete(Constants.LogPath);
            }
            File.Create(Constants.LogPath);
        }

        private void RedrawBoard()
        {
            for (int row = 0; row < Constants.Rows; row++)
            {
                for (int column = 0; column < Constants.Columns; column++)
                {
                    CellsButtons[row, column].Background = CellToColor(Tunnel.Cells[row, column]);
                }
            }
        }

        private async void Loop()
        {
            Tunnel.CalculateDistancesToExits();
            Tunnel.CalculateDistancesToObstacles();
            Pedestrian.InitPedestrians();
            int timeStepCounter = 0;
            while (true)
            {
                await Task.Delay((int) (1000.0 * Constants.TimeStep * 5));
                if (!paused)
                {
                    double currentTime = timeStepCounter * Constants.TimeStep;
                    Timer.Content = string.Format("{0} s", Math.Round(timeStepCounter * Constants.TimeStep, 1));
                    Tunnel.UpdateCells();
                    if (currentTime >= Constants.EvacuationDelay)
                    {
                        Tunnel.UpdatePedestrians();
                    }
                    RedrawBoard();
                    timeStepCounter++;       
                }
            }
        }

        public static SolidColorBrush CellToColor(TunnelCell cell)
        {
            if (cell.PedestrianPresent)
            {
                return Brushes.Pink;
            }
            if (cell.CellType == ECellType.clear)
            {
                return new SolidColorBrush(Color.FromArgb((byte)Math.Floor(192.0 * cell.SmokeConcentration), 0, 0, 0));
            }
            else
            {
                return CellTypeToColor(cell.CellType);
            }
        }

        private static SolidColorBrush CellTypeToColor(ECellType cellType)
        {
            switch (cellType)
            {
                case ECellType.smokeSource:
                    return Brushes.DarkOrange;
                case ECellType.obstacle:
                    return Brushes.Black;
                case ECellType.exit:
                    return Brushes.Green;
                case ECellType.pedestrian:
                    return Brushes.Pink;
                default:
                    return Brushes.White;
            }
        }

        private void LoadClicked(object sender, RoutedEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "CSV Files (*.csv)|*.csv";
            if (openFileDialog.ShowDialog() == true)
            {
                using (TextFieldParser parser = new TextFieldParser(openFileDialog.FileName))
                {
                    parser.TextFieldType = FieldType.Delimited;
                    parser.SetDelimiters(",");
                    for (int row = 0; row < Constants.Rows; row++)
                    {
                        string[] columns = parser.ReadFields();
                        for (int column = 0; column < Constants.Columns; column++)
                        {
                            Tunnel.Cells[row, column].CellType = (ECellType) Enum.Parse(typeof(ECellType), columns[column]);
                        }
                    }
                }
                RedrawBoard();
            }
        }

        private void SaveClicked(object sender, RoutedEventArgs e)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV Files (*.csv)|*.csv";
            if (saveFileDialog.ShowDialog() == true)
            {
                StringBuilder sb = new StringBuilder();
                for (int row = 0; row < Constants.Rows; row++)
                {
                    for (int column = 0; column < Constants.Columns; column++)
                    {
                        sb.Append(Tunnel.Cells[row, column].CellType);
                        if (column < Constants.Columns - 1)
                        {
                            sb.Append(",");
                        }
                    }
                    if (row < Constants.Rows - 1)
                    {
                        sb.Append("\n");
                    }
                }
                File.WriteAllText(saveFileDialog.FileName, sb.ToString());
            }
        }

        private void StartClicked(object sender, RoutedEventArgs e)
        {
            ButtonLoad.IsEnabled = false;
            ButtonSave.IsEnabled = false;
            ButtonStart.IsEnabled = false;
            Loop();
        }

        private void PauseClicked(object sender, RoutedEventArgs e)
        {
            paused = !paused;
        }

        private void CellButtonLeftMouseDown(object sender, MouseButtonEventArgs e)
        {
            Border source = sender as Border;
            int row = (int)source.GetValue(Grid.RowProperty);
            int column = (int)source.GetValue(Grid.ColumnProperty);
            ECellType cellType = (ECellType)CellTypeChoice.SelectedItem;
            Tunnel.Cells[row, column].CellType = cellType;
            source.Background = CellTypeToColor(cellType);
        }

        private void CellButtonMouseEnter(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Border source = sender as Border;
                int row = (int)source.GetValue(Grid.RowProperty);
                int column = (int)source.GetValue(Grid.ColumnProperty);
                ECellType cellType = (ECellType)CellTypeChoice.SelectedItem;
                Tunnel.Cells[row, column].CellType = cellType;
                source.Background = CellTypeToColor(cellType);
            }
        }
    }
}
