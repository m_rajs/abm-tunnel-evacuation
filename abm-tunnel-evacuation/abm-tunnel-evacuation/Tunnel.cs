﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace abm_tunnel_evacuation
{
    class Tunnel
    {
        public TunnelCell[,] Cells { get; }
        public List<Pedestrian> Pedestrians { get; }

        private static Tunnel tunnel;
        private readonly int rows;
        private readonly int columns;

        public static Tunnel GetTunnel()
        {
            if (tunnel == null)
                tunnel = new Tunnel(Constants.Rows, Constants.Columns);
            return tunnel;
        }

        private Tunnel(int rows, int columns)
        {
            this.rows = rows;
            this.columns = columns;
            Cells = new TunnelCell[rows, columns];
            Pedestrians = new List<Pedestrian>();
            InitializeCells();
            InitializeNeighborhoods();
        }

        public void CalculateDistancesToExits()
        {
            List<TunnelCell> toCheck = new List<TunnelCell>();
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    if (Cells[row, column].CellType == ECellType.exit)
                    {
                        Cells[row, column].DistanceToExit = 0;
                        toCheck.AddRange(Cells[row, column].Neighbours);
                    }
                }
            }
            while (toCheck.Any())
            {             
                if (toCheck[0].CalculateDistanceToExit())
                {
                    toCheck.AddRange(toCheck[0].Neighbours);
                }
                toCheck.RemoveAt(0);
            }
        }

        public void CalculateDistancesToObstacles()
        {
            List<TunnelCell> toCheck = new List<TunnelCell>();
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    if (Cells[row, column].CellType == ECellType.obstacle)
                    {
                        Cells[row, column].DistanceToObstacle = 0;
                        toCheck.AddRange(Cells[row, column].Neighbours);
                    }
                }
            }
            while (toCheck.Any())
            {
                if (toCheck[0].CalculateDistanceToObstacle())
                {
                    toCheck.AddRange(toCheck[0].Neighbours);
                }
                toCheck.RemoveAt(0);
            }
        }

        public void UpdateCells()
        {
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    Cells[row, column].CalculateNextSmokeConcentration();
                }
            }
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    Cells[row, column].UpdateSmokeConcentration();
                }
            }
        }

        public void UpdatePedestrians()
        {
            Random r = new Random();
            List<Pedestrian> sortedPedestrians = Pedestrians.OrderByDescending(x => x.ActualVelocity).ToList();
            foreach (Pedestrian pedestrian in sortedPedestrians)
            {
                pedestrian.TimeInSimulation += Constants.TimeStep;
                double moveProbability = r.NextDouble();
                if (moveProbability <= pedestrian.ActualVelocity / Constants.MaximumDesiredVelocity)
                {
                    pedestrian.PickNextLocation();
                    pedestrian.Move();
                }
            }
        }

        private void InitializeCells()
        {
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    Cells[row, column] = new TunnelCell();
                    Cells[row, column].X_pos = row;
                    Cells[row, column].Y_pos = column;
                }
            }
        }

        private void InitializeNeighborhoods()
        {
            for (int row = 0; row < rows; row++)
            {
                for (int column = 0; column < columns; column++)
                {
                    if (row > 0)
                    {
                        if (column > 0)
                        {
                            Cells[row, column].Neighbours.Add(Cells[row - 1, column - 1]);
                        }
                        Cells[row, column].Neighbours.Add(Cells[row - 1, column]);
                        if (column < columns - 1)
                        {
                            Cells[row, column].Neighbours.Add(Cells[row - 1, column + 1]);
                        }
                    }
                    if (column > 0)
                    {
                        Cells[row, column].Neighbours.Add(Cells[row, column - 1]);
                    }
                    if (column < columns - 1)
                    {
                        Cells[row, column].Neighbours.Add(Cells[row, column + 1]);
                    }
                    if (row < rows - 1)
                    {
                        if (column > 0)
                        {
                            Cells[row, column].Neighbours.Add(Cells[row + 1, column - 1]);
                        }
                        Cells[row, column].Neighbours.Add(Cells[row + 1, column]);
                        if (column < columns - 1)
                        {
                            Cells[row, column].Neighbours.Add(Cells[row + 1, column + 1]);
                        }
                    }
                }
            }
        }
    }
}
