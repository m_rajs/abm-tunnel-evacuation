﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abm_tunnel_evacuation
{
    class Constants
    {
        public const int BoardHeight = 100;
        public const int BoardWidth = 400;
        public const int Rows = 10;
        public const int Columns = 40;

        public const double MaxDistanceToExit = 400;
        public const double MaxDistanceToObstacle = 400;

        // Cell length/width in meters.
        public const double CellSize = 0.5;
        // The maximum possible desired velocity in m/s.
        public const double MaximumDesiredVelocity = 3.0;
        // Time step in seconds.
        public const double TimeStep = CellSize / MaximumDesiredVelocity;

        // Time needed for pedestrians to start evacuating (in seconds).
        public const double EvacuationDelay = 15;

        public const double SmokeVelocity = 0.05;

        public const double DistanceToExitWeight = 1.0;
        public const double DistanceToObstacleWeight = 0.5;
        public const double SmokeConcentrationWeight = 0.2;
        public const double PedestrianNearbyWeight = 0.7;

        public const string LogPath = "log.txt";
    }
}
