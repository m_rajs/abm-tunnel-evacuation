﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abm_tunnel_evacuation
{
    public class Pedestrian
    {
        public TunnelCell Location;

        public double DesiredVelocity;
        public double ActualVelocity => (DesiredVelocity - DesiredVelocity * 0.5 * (1.0 - endurance) * Location.SmokeConcentration);

        public double TimeInSimulation;

        private TunnelCell nextLocation;
        private double endurance;

        public Pedestrian(TunnelCell initialPosition)
        {
            Location = initialPosition;
            TimeInSimulation = 0.0;
            nextLocation = Location;
            Random r = new Random();
            int age = r.Next(20, 80);
            int sex = r.Next(2);
            double fitnessLevel = r.NextDouble() * 2.0 - 1.0;
            endurance = r.NextDouble();
            if (sex == 0)
            {
                if (age < 40)
                {
                    DesiredVelocity = 1.43 + fitnessLevel * 0.12;
                }
                else if (age < 60)
                {
                    DesiredVelocity = 1.42 + fitnessLevel * 0.19;
                }
                else
                {
                    DesiredVelocity = 1.34 + fitnessLevel * 0.2;
                }
            }
            else
            {
                if (age < 40)
                {
                    DesiredVelocity = 1.41 + fitnessLevel * 0.14;
                }
                else if (age < 60)
                {
                    DesiredVelocity = 1.39 + fitnessLevel * 0.15;
                }
                else
                {
                    DesiredVelocity = 1.28 + fitnessLevel * 0.21;
                }
            }
        }

        public static void InitPedestrians()
        {
            foreach (var cell in Tunnel.GetTunnel().Cells)
            {
                if (cell.CellType == ECellType.pedestrian)
                {
                    cell.CellType = ECellType.clear;
                    cell.PedestrianPresent = true;
                    Tunnel.GetTunnel().Pedestrians.Add(new Pedestrian(cell));
                }
            }
        }

        public void PickNextLocation()
        {
            List<TunnelCell> candidates = Location.Neighbours
                ?.Where(x => x.CellType != ECellType.obstacle && x.CellType != ECellType.smokeSource && !x.PedestrianPresent)
                ?.ToList();
            List<double> scores = candidates
                ?.Select(x => Constants.DistanceToExitWeight * (x.DistanceToExit / Constants.MaxDistanceToExit)
                              - Constants.DistanceToObstacleWeight * (x.DistanceToObstacle / Constants.MaxDistanceToObstacle)
                              + Constants.SmokeConcentrationWeight * x.SmokeConcentration
                              + Constants.PedestrianNearbyWeight * x.IsPedestrianNearby())
                ?.ToList();
            List<TunnelCell> bestCandidates = new List<TunnelCell>();
            for (int i = 0; i < scores.Count(); i++)
            {
                if (Math.Abs(scores[i] - scores.Min()) < 0.0001)
                {
                    bestCandidates.Add(candidates[i]);
                }
            }
            if (bestCandidates.Any())
            {
                Random r = new Random();
                int i = r.Next(bestCandidates.Count());
                nextLocation = bestCandidates[i];
            } 
            else
            {
                nextLocation = Location;
            }
        }

        public void Move()
        {
            var mainWindow = App.Current.MainWindow as MainWindow;

            Location.PedestrianPresent = false;

            if (nextLocation.CellType == ECellType.exit)
            {
                if (mainWindow != null)
                {
                    mainWindow.CellsButtons[Location.X_pos, Location.Y_pos].Background = MainWindow.CellToColor(Location);
                    mainWindow.LogPedestrian(this);
                }
                Location = nextLocation;
                nextLocation = Location;
                Tunnel.GetTunnel().Pedestrians.Remove(this);
                return;
            }

            nextLocation.PedestrianPresent = true;

            if (mainWindow != null)
            {
                mainWindow.CellsButtons[Location.X_pos, Location.Y_pos].Background = MainWindow.CellToColor(Location);
                mainWindow.CellsButtons[nextLocation.X_pos, nextLocation.Y_pos].Background = MainWindow.CellToColor(nextLocation);
            }
            
            Location = nextLocation;
            nextLocation = Location;
        }
    }
}
